FROM ubuntu:latest

RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*


RUN wget https://updates.peer2profit.com/p2pclient_0.56_amd64.deb


RUN dpkg -i p2pclient_0.56_amd64.deb

